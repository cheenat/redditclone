package user

import (
	"fmt"
	"reflect"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

// go test -coverprofile=cover.out && go tool cover -html=cover.out -o cover.html

func TestGetByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	_ = mock
	if err != nil {
		t.Fatalf("cant create mock: %s", err)
	}
	defer db.Close()

	elemID := "1"

	// 	// good query
	rows := sqlmock.
		NewRows([]string{"Login", "Password"})
	expect := []*User{
		{0, "cheena", "strgpsswrd"},
	}
	for _, item := range expect {
		rows = rows.AddRow(item.Login, item.Password)
		fmt.Println("\n rows", rows)
	}
	mock.
		ExpectQuery("SELECT login, password FROM users WHERE").
		WithArgs(elemID).
		WillReturnRows(rows)

	repo := &UserRepo{
		DB: db,
	}
	item, err := repo.FindUser(elemID)
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if !reflect.DeepEqual(item, expect[0]) {
		t.Errorf("results not match, want %v, have %v", expect[0], item)
		return
	}

	// query error
	mock.
		ExpectQuery("SELECT login, password FROM users WHERE").
		WithArgs(elemID).
		WillReturnError(fmt.Errorf("db_error"))

	_, err = repo.FindUser(elemID)
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}
}

func TestCreate(t *testing.T) {
	db, mock, err := sqlmock.New()
	_ = mock
	if err != nil {
		t.Fatalf("cant create mock: %s", err)
	}
	defer db.Close()

	repo := NewUserRepo(db)

	title := "title"
	descr := "description"
	testItem := &User{
		Login:    title,
		Password: descr,
	}
	_ = testItem

	//ok query

	tokenString, err := repo.Authorize("1", "cheena")
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}
	if tokenString == "" {
		t.Errorf("empty token")
		return
	}
}

func TestRegistration(t *testing.T) {
	db, mock, err := sqlmock.New()
	_ = mock
	if err != nil {
		t.Fatalf("cant create mock: %s", err)
	}
	defer db.Close()

	repo := NewUserRepo(db)

	rows := sqlmock.
		NewRows([]string{"Login", "Password"})
	_ = rows

	elemID := 1
	_ = elemID

	mock.ExpectExec("INSERT").
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg()).
		WillReturnResult(sqlmock.NewResult(1, 1))

	_, _, RegisterError := repo.Registration("cheena", "strgpsswrd")
	if RegisterError != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}

	// error query

	rows = sqlmock.NewRows([]string{"Login"})
	rows = rows.AddRow("cheena")

	mock.
		ExpectQuery("SELECT login FROM users WHERE login = ?").
		WithArgs("cheena").
		WillReturnRows(rows)

	_, _, RegisterError = repo.Registration("cheena", "strgpsswrd")
	if RegisterError == nil {
		t.Errorf("expected error, got nil")
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}

	// error query

	mock.
		ExpectQuery("SELECT login FROM users WHERE login = ?").
		WithArgs("cheena").
		WillReturnError(fmt.Errorf("db_error"))

	_, _, RegisterError = repo.Registration("cheena", "strgpsswrd")
	if RegisterError == nil {
		t.Errorf("expected error, got nil")
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}

	//

	rows = sqlmock.NewRows([]string{"Login"})
	rows = rows.AddRow("")

	mock.
		ExpectQuery("SELECT login FROM users WHERE login = ?").
		WithArgs("cheena").
		WillReturnRows(rows)

	result := sqlmock.NewErrorResult(fmt.Errorf("no RowsAffected/LastInsertId available"))
	mock.ExpectExec("INSERT").WillReturnResult(result)

	_, _, RegisterError = repo.Registration("cheena", "strgpsswrd")
	if RegisterError == nil {
		t.Errorf("expected error, got nil")
		return
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}

}
