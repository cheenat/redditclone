package user

import (
	"database/sql"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID       int
	Login    string
	Password string
}

type UserRepo struct {
	DB *sql.DB
	mu *sync.RWMutex
}

type Error struct {
	Location string `json:"location"`
	Msg      string `json:"msg"`
	Param    string `json:"param"`
	Value    string `json:"value"`
}

func NewUserRepo(db *sql.DB) *UserRepo {
	return &UserRepo{
		mu: &sync.RWMutex{},
		DB: db,
	}
}

var (
	ErrNoUser         = errors.New("No user found")
	ErrBadPass        = errors.New("Invald password")
	ErrBadPassOrLogin = errors.New("invalid login or password")
)

func (repo *UserRepo) Authorize(sessionUserID, login string) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": jwt.MapClaims{"username": login, "id": sessionUserID},
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(time.Hour * 10).Unix(),
	})
	tokenString, err := token.SignedString([]byte("супер секретный ключ"))
	if err != nil {
		// err := []*Error{&Error{Location: "body", Msg: err.Error(), Param: "username", Value: login}}
		return "", err
	}

	return tokenString, nil
}

func (repo *UserRepo) Registration(login, pass string) (string, string, []*Error) {
	// fmt.Println("\nregistration login and password", login, pass, "\n")
	dbLogin := ""
	userID := ""
	sqlStmt := `SELECT login FROM users WHERE login = ?`

	_ = repo.DB.QueryRow(sqlStmt, login).Scan(&dbLogin) // errDB
	// if dbLogin != "" || errDB != nil {
	if dbLogin != "" {
		fmt.Println("\n dbLogin not empty")
		err := []*Error{&Error{Location: "body", Msg: "already exists", Param: "username", Value: login}}
		return "", "", err
	} else {
		fmt.Println("\n dbLogin is empty")
		objectID := primitive.NewObjectID()
		userID = objectID.Hex()

		result, err := repo.DB.Exec(
			"INSERT INTO users (`login`, `password`, `userID`) VALUES (?, ?, ?)",
			login,
			pass,
			userID,
		)
		if err != nil {
			fmt.Println(result, err)
			err := []*Error{&Error{Location: "body", Msg: err.Error(), Param: "username", Value: login}}
			return "", "", err
		}

		affected, err := result.RowsAffected()
		if err != nil {
			err := []*Error{&Error{Location: "body", Msg: err.Error(), Param: "username", Value: login}}
			return "", "", err
		}
		lastID, err := result.LastInsertId()
		if err != nil {
			err := []*Error{&Error{Location: "body", Msg: err.Error(), Param: "username", Value: login}}
			return "", "", err
		}
		fmt.Println("Insert - RowsAffected", affected, "LastInsertId: ", lastID)
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": jwt.MapClaims{"username": login, "id": userID},
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(time.Hour * 10).Unix(),
	})
	tokenString, err := token.SignedString([]byte("супер секретный ключ"))
	if err != nil {
		err := []*Error{&Error{Location: "body", Msg: err.Error(), Param: "username", Value: login}}
		fmt.Println(err)
	}

	return tokenString, userID, nil
}

func (repo *UserRepo) FindUser(userID string) (*User, error) {
	var user User
	err := repo.DB.QueryRow(`SELECT login, password FROM users WHERE userID = ?`, userID).Scan(&user.Login, &user.Password)
	if err != nil {
		return nil, err
	}
	return &user, nil
}
