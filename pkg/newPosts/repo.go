package newPosts

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

const collectionName = "posts"

type PostsDatabaseI interface {
	GetSpecificPosts(context.Context, interface{}) ([]*Post, error)
	Create(context.Context, *Post) (interface{}, error)
	FindOne(context.Context, interface{}) (*Post, error)
	DeleteByID(ctx context.Context, filter interface{}) (int64, error)
	UpdatePost(ctx context.Context, filter interface{}, update interface{}) (*mongo.UpdateResult, error)
}

type PostsDatabase struct {
	db DatabaseHelper
}

func NewPostDatabase(db DatabaseHelper) PostsDatabaseI {
	return &PostsDatabase{
		db: db,
	}
}

func (u *PostsDatabase) GetSpecificPosts(ctx context.Context, filter interface{}) ([]*Post, error) {

	posts := []*Post{}

	cursor := u.db.Collection(collectionName).Find(ctx, filter)

	if err := cursor.All(context.TODO(), &posts); err != nil {
		return nil, err
	}

	return posts, nil
}

func (u *PostsDatabase) FindOne(ctx context.Context, filter interface{}) (*Post, error) {
	post := &Post{}

	err := u.db.Collection(collectionName).FindOne(ctx, filter).Decode(post)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (u *PostsDatabase) Create(ctx context.Context, post *Post) (interface{}, error) {
	res, err := u.db.Collection(collectionName).InsertOne(ctx, post)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (u *PostsDatabase) DeleteByID(ctx context.Context, filter interface{}) (int64, error) {
	res, err := u.db.Collection(collectionName).DeleteOne(context.Background(), filter)
	if err != nil {
		return 0, err
	}

	return res, nil
}

func (p *PostsDatabase) UpdatePost(ctx context.Context, filter interface{}, update interface{}) (*mongo.UpdateResult, error) {
	res, err := p.db.Collection(collectionName).UpdateOne(context.Background(), filter, update)
	if err != nil {
		return nil, err
	}
	return res, nil
}
