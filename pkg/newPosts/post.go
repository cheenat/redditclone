package newPosts

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Author struct {
	ID       primitive.ObjectID `json:"id" bson:"_id"`
	Username string             `json:"username" bson:"username"`
}

type Comment struct {
	ID      primitive.ObjectID `json:"id" bson:"_id"`
	Author  *Author            `json:"author" bson:"author"`
	Body    string             `json:"body" bson:"body"`
	Created string             `json:"created" bson:"created"`
}

type Post struct {
	Author           *Author             `json:"author" bson:"author"`
	Category         string              `json:"category" bson:"category"`
	Created          string              `json:"created" bson:"created"`
	Comments         []*Comment          `json:"comments" bson:"comments"`
	ID               primitive.ObjectID  `json:"id" bson:"_id"`
	Score            int                 `json:"score" bson:"score"`
	Text             string              `json:"text" bson:"text"`
	Title            string              `json:"title" bson:"title"`
	Type             string              `json:"type" bson:"type"`
	UpvotePercentage int                 `json:"upvotePercentage" bson:"upvotePercentage"`
	Views            int                 `json:"views" bson:"views"`
	Votes            []map[string]string `json:"votes" bson:"votes"`
	Url              string              `json:"url" bson:"url"`
}
