package newPosts

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DatabaseHelper interface {
	Collection(name string) CollectionHelper
}

type CollectionHelper interface {
	Find(context.Context, interface{}) CursorResultHelper
	FindOne(context.Context, interface{}) SingleResultHelper
	InsertOne(context.Context, interface{}) (interface{}, error)
	DeleteOne(ctx context.Context, filter interface{}) (int64, error)
	UpdateOne(ctx context.Context, filter, update interface{}) (*mongo.UpdateResult, error)
}

type CursorResultHelper interface {
	All(ctx context.Context, results interface{}) error
}

type SingleResultHelper interface {
	Decode(v interface{}) error
}

type ClientHelper interface {
	Database(string) DatabaseHelper
	Connect() error
	StartSession() (mongo.Session, error)
}

type mongoClient struct {
	cl *mongo.Client
}
type MongoDatabase struct {
	DB *mongo.Database
}
type mongoCollection struct {
	coll *mongo.Collection
}

type mongoCursorResult struct {
	cursor *mongo.Cursor
}

type mongoSingleResult struct {
	sr *mongo.SingleResult
}

type mongoSession struct {
	mongo.Session
}

type Config struct {
	Username     string
	Password     string
	DatabaseName string
	URL          string
}

func GetConfig() *Config {
	return &Config{
		Username:     "cheena",
		Password:     "1234",
		DatabaseName: "mockredditclone",
		URL:          "mongodb://127.0.0.1:27017",
	}
}

func NewClient(cnf *Config) (ClientHelper, error) {
	credential := options.Credential{
		Username: "cheenaa",
		Password: "1234",
	}
	clientOpts := options.Client().ApplyURI("mongodb://127.0.0.1:27017").SetAuth(credential)
	c, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		log.Fatal(err)
	}

	return &mongoClient{cl: c}, err

}

func NewDatabase(cnf *Config, client ClientHelper) DatabaseHelper {
	return client.Database(cnf.DatabaseName)
}

func (mc *mongoClient) Database(dbName string) DatabaseHelper {
	db := mc.cl.Database(dbName)
	return &MongoDatabase{DB: db}
}

func (mc *mongoClient) StartSession() (mongo.Session, error) {
	session, err := mc.cl.StartSession()
	return &mongoSession{session}, err
}

func (mc *mongoClient) Connect() error {
	return mc.cl.Connect(nil)
}

func (md *MongoDatabase) Collection(colName string) CollectionHelper {
	collection := md.DB.Collection(colName)
	return &mongoCollection{coll: collection}
}

func (md *MongoDatabase) Client() ClientHelper {
	client := md.DB.Client()
	return &mongoClient{cl: client}
}

func (mc *mongoCollection) Find(ctx context.Context, filter interface{}) CursorResultHelper {
	cursorResult, _ := mc.coll.Find(ctx, filter)
	return &mongoCursorResult{cursor: cursorResult}
}

func (mc *mongoCollection) FindOne(ctx context.Context, filter interface{}) SingleResultHelper {
	singleResult := mc.coll.FindOne(ctx, filter)
	return &mongoSingleResult{sr: singleResult}
}

func (mc *mongoCollection) InsertOne(ctx context.Context, document interface{}) (interface{}, error) {
	id, err := mc.coll.InsertOne(ctx, document)
	return id.InsertedID, err
}

func (mc *mongoCollection) DeleteOne(ctx context.Context, filter interface{}) (int64, error) {
	count, err := mc.coll.DeleteOne(ctx, filter)
	return count.DeletedCount, err
}

func (mc *mongoCollection) UpdateOne(ctx context.Context, filter, update interface{}) (*mongo.UpdateResult, error) {
	count, err := mc.coll.UpdateOne(ctx, filter, update)
	return count, err
}

func (cur *mongoCursorResult) All(ctx context.Context, results interface{}) error {
	return cur.cursor.All(ctx, results)
}

func (sr *mongoSingleResult) Decode(v interface{}) error {
	return sr.sr.Decode(v)
}
