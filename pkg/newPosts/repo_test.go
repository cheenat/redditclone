package newPosts_test

import (
	"context"
	"errors"
	"testing"

	"rc/pkg/newPosts"
	"rc/pkg/newPosts/mocks"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestNewPostDatabase(t *testing.T) {
	conf := newPosts.GetConfig()

	dbClient, err := newPosts.NewClient(conf)
	assert.NoError(t, err)

	db := newPosts.NewDatabase(conf, dbClient)

	userDB := newPosts.NewPostDatabase(db)

	assert.NotEmpty(t, userDB)
}

func TestCreate(t *testing.T) {

	var dbHelper newPosts.DatabaseHelper
	var dbHelperCorrect newPosts.DatabaseHelper
	var errCollectionHelper newPosts.CollectionHelper
	var CorrectCollectionHelper newPosts.CollectionHelper

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperCorrect = &mocks.DatabaseHelper{}
	errCollectionHelper = &mocks.CollectionHelper{}
	CorrectCollectionHelper = &mocks.CollectionHelper{}

	errCollectionHelper.(*mocks.CollectionHelper).
		On("InsertOne", context.Background(), mock.AnythingOfType("*newPosts.Post")).
		Return(nil, errors.New("mocked-error"))

	expectedInsertedID := &mongo.InsertOneResult{}
	CorrectCollectionHelper.(*mocks.CollectionHelper).
		On("InsertOne", context.Background(), mock.AnythingOfType("*newPosts.Post")).
		Return(expectedInsertedID.InsertedID, nil)

	dbHelper.(*mocks.DatabaseHelper).
		On("Collection", "posts").Return(errCollectionHelper)

	userDba := newPosts.NewPostDatabase(dbHelper)

	post := &newPosts.Post{}
	post.Author = &newPosts.Author{ID: primitive.NewObjectID(), Username: "mocked-user"}
	_, err := userDba.Create(context.Background(), post)

	assert.EqualError(t, err, "mocked-error")

	dbHelperCorrect.(*mocks.DatabaseHelper).
		On("Collection", "posts").Return(CorrectCollectionHelper)
	userDba = newPosts.NewPostDatabase(dbHelperCorrect)

	post = &newPosts.Post{}
	post.Author = &newPosts.Author{ID: primitive.NewObjectID(), Username: "rvasily"}
	insertedID, err := userDba.Create(context.Background(), post)
	assert.Equal(t, expectedInsertedID.InsertedID, insertedID)
	assert.NoError(t, err)
}

func TestFindOne(t *testing.T) {

	var dbHelper newPosts.DatabaseHelper
	var collectionHelper newPosts.CollectionHelper
	var srHelperErr newPosts.SingleResultHelper
	var srHelperCorrect newPosts.SingleResultHelper

	dbHelper = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.CollectionHelper{}
	srHelperErr = &mocks.SingleResultHelper{}
	srHelperCorrect = &mocks.SingleResultHelper{}

	srHelperErr.(*mocks.SingleResultHelper).
		On("Decode", mock.AnythingOfType("*newPosts.Post")).
		Return(errors.New("mocked-error"))

	srHelperCorrect.(*mocks.SingleResultHelper).
		On("Decode", mock.AnythingOfType("*newPosts.Post")).
		Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(*newPosts.Post)
		author := &newPosts.Author{Username: "mocked-user"}
		arg.Author = author
	})

	collectionHelper.(*mocks.CollectionHelper).
		On("FindOne", context.Background(), bson.M{"error": true}).
		Return(srHelperErr)

	collectionHelper.(*mocks.CollectionHelper).
		On("FindOne", context.Background(), bson.M{"error": false}).
		Return(srHelperCorrect)

	dbHelper.(*mocks.DatabaseHelper).
		On("Collection", "posts").Return(collectionHelper)

	userDba := newPosts.NewPostDatabase(dbHelper)

	post, err := userDba.FindOne(context.Background(), bson.M{"error": true})

	assert.Empty(t, post)
	assert.EqualError(t, err, "mocked-error")

	post, err = userDba.FindOne(context.Background(), bson.M{"error": false})

	assert.Equal(t, &newPosts.Post{Author: &newPosts.Author{Username: "mocked-user"}}, post)
	assert.NoError(t, err)
}

func TestDeleteByID(t *testing.T) {

	var dbHelper newPosts.DatabaseHelper
	var collectionHelper newPosts.CollectionHelper

	dbHelper = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.CollectionHelper{}

	var errResult, correctResult int64
	errResult = 0
	correctResult = 1

	collectionHelper.(*mocks.CollectionHelper).
		On("DeleteOne", context.Background(), bson.M{"error": false}).
		Return(errResult, errors.New("mocked-error"))

	collectionHelper.(*mocks.CollectionHelper).
		On("DeleteOne", context.Background(), bson.M{"error": true}).
		Return(correctResult, nil)

	dbHelper.(*mocks.DatabaseHelper).
		On("Collection", "posts").Return(collectionHelper)

	userDba := newPosts.NewPostDatabase(dbHelper)
	_, err := userDba.DeleteByID(context.Background(), bson.M{"error": false})
	assert.EqualError(t, err, "mocked-error")

	userDba = newPosts.NewPostDatabase(dbHelper)
	res, err := userDba.DeleteByID(context.Background(), bson.M{"error": true})
	assert.Equal(t, int64(1), res)
	assert.NoError(t, err)
}

func TestUpdatePost(t *testing.T) {

	var dbHelper newPosts.DatabaseHelper
	var collectionHelper newPosts.CollectionHelper

	dbHelper = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.CollectionHelper{}

	var notPrimitiveObjID, correctResult int64
	notPrimitiveObjID = 0
	correctResult = 1
	_ = correctResult
	errorFilter := bson.D{{"_id", notPrimitiveObjID}}
	errorUpdate := bson.D{
		{"$pull",
			bson.D{
				{"comments",
					bson.M{"_id": primitive.NewObjectID()},
				},
			},
		},
	}
	correctFilter := bson.D{{"_id", primitive.NewObjectID()}}
	correctUpdate := bson.D{
		{"$pull",
			bson.D{
				{"comments",
					bson.M{"_id": primitive.NewObjectID()},
				},
			},
		},
	}

	collectionHelper.(*mocks.CollectionHelper).
		On("UpdateOne", context.Background(), errorFilter, errorUpdate).
		Return(nil, errors.New("mocked-error"))

	updateResults := &mongo.UpdateResult{MatchedCount: 1, ModifiedCount: 1}
	_ = updateResults
	collectionHelper.(*mocks.CollectionHelper).
		On("UpdateOne", context.Background(), correctFilter, correctUpdate).
		Return(updateResults, nil)

	dbHelper.(*mocks.DatabaseHelper).
		On("Collection", "posts").Return(collectionHelper)

	userDba := newPosts.NewPostDatabase(dbHelper)
	_, err := userDba.UpdatePost(context.Background(), errorFilter, errorUpdate)
	assert.EqualError(t, err, "mocked-error")

	userDba = newPosts.NewPostDatabase(dbHelper)
	res, err := userDba.UpdatePost(context.Background(), correctFilter, correctUpdate)
	assert.Equal(t, updateResults, res)
	assert.NoError(t, err)
}

func TestGetSpecificPosts(t *testing.T) {

	var dbHelper newPosts.DatabaseHelper
	var collectionHelper newPosts.CollectionHelper
	var curHelperErr newPosts.CursorResultHelper
	var curHelperCorrect newPosts.CursorResultHelper

	dbHelper = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.CollectionHelper{}
	curHelperErr = &mocks.CursorResultHelper{}
	curHelperCorrect = &mocks.CursorResultHelper{}

	curHelperErr.(*mocks.CursorResultHelper).
		On("All", mock.Anything, mock.Anything).
		Return(errors.New("mocked-error"))

	curHelperCorrect.(*mocks.CursorResultHelper).
		On("All", mock.Anything, mock.AnythingOfType("*[]*newPosts.Post")).
		Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(1).(*[]*newPosts.Post)
		post := &newPosts.Post{}
		author := &newPosts.Author{Username: "mocked-user"}
		post.Author = author
		_ = arg
		*arg = append(*arg, post)
	})

	collectionHelper.(*mocks.CollectionHelper).
		On("Find", context.Background(), bson.M{"error": true}).
		Return(curHelperErr)

	collectionHelper.(*mocks.CollectionHelper).
		On("Find", context.Background(), bson.M{"error": false}).
		Return(curHelperCorrect)

	dbHelper.(*mocks.DatabaseHelper).
		On("Collection", "posts").Return(collectionHelper)

	userDba := newPosts.NewPostDatabase(dbHelper)

	post, err := userDba.GetSpecificPosts(context.Background(), bson.M{"error": true})

	assert.Empty(t, post)
	assert.EqualError(t, err, "mocked-error")

	post, err = userDba.GetSpecificPosts(context.Background(), bson.M{"error": false})
	posts := []*newPosts.Post{}
	author := &newPosts.Author{Username: "mocked-user"}
	expectedPost := &newPosts.Post{Author: author}
	posts = append(posts, expectedPost)
	assert.Equal(t, posts, post)
	assert.NoError(t, err)
}
