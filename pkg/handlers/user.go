package handlers

import (
	"database/sql"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"rc/pkg/session"
	"rc/pkg/user"

	uuid "github.com/satori/go.uuid"
	"go.uber.org/zap"
)

//go:generate mockgen -source=user.go -destination=user_mock.go -package=handlers UserRepositoryInterface

type UserRepositoryInterface interface {
	Authorize(string, string) (string, error)
	Registration(string, string) (string, string, []*user.Error)
	FindUser(string) (*user.User, error)
}

type SessionRepositoryInterface interface {
	Create(http.ResponseWriter, string, string) error
	FindSession(string) (*session.SQLSession, error)
	InsertSession(string, string, string) error
}

type HandlerDB struct {
	// UserRepo *user.UserRepo
	UserRepo UserRepositoryInterface
	Logger   *zap.SugaredLogger
	// Sessions *session.SessionsManager
	Sessions SessionRepositoryInterface
}

type LoginForm struct {
	Login    string `json:"username"`
	Password string `json:"password"`
}

func (h *HandlerDB) Register(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()

	fd := &LoginForm{}
	err := json.Unmarshal(body, fd)
	if err != nil {
		http.Error(w, `cant unpack payload`, http.StatusBadRequest)
		return
	}

	tokenString, userID, RegisterError := h.UserRepo.Registration(fd.Login, fd.Password)
	if RegisterError != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		resp, errMarshal := json.Marshal(map[string][]*user.Error{"errors": RegisterError})
		if errMarshal != nil {
			http.Error(w, `Marshaling Error`, http.StatusInternalServerError)
			return
		}
		w.Write(resp)
		return
	}

	err = h.Sessions.Create(w, userID, fd.Login)
	if err != nil {
		http.Error(w, `DB err`, http.StatusInternalServerError)
		return
	}
	// h.Logger.Infof("created session %s for user %s", sess, userID)

	resp, err := json.Marshal(map[string]interface{}{
		"token": tokenString,
	})
	if err != nil {
		http.Error(w, `Marshaling Error`, http.StatusInternalServerError)
		return
	}

	w.Write(resp)
}

func (h *HandlerDB) Login(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()

	fd := &LoginForm{}
	err := json.Unmarshal(body, fd)
	if err != nil {
		http.Error(w, `cant unpack payload`, http.StatusBadRequest)
		return
	}

	sessionCookie, err := r.Cookie("session_id")
	if err != http.ErrNoCookie && sessionCookie.Value != "" {
		session, err := h.Sessions.FindSession(sessionCookie.Value)
		if err != nil {
			queryRowErrorHandler(err, w)
		} else if session.UserID != "" {
			u, err := h.UserRepo.FindUser(session.UserID)
			if err != nil {
				queryRowErrorHandler(err, w)
				return
			}
			if u.Login == fd.Login && u.Password == fd.Password {
				sessID := uuid.NewV4()
				cookie := &http.Cookie{
					Name:    "session_id",
					Value:   sessID.String(),
					Expires: time.Now().Add(10 * time.Hour),
					Path:    "/",
				}
				http.SetCookie(w, cookie)

				err := h.Sessions.InsertSession(sessID.String(), session.UserID, fd.Login)
				if err != nil {
					http.Error(w, `DB err`, http.StatusInternalServerError)
					return
				}

				tokenString, err := h.UserRepo.Authorize(session.UserID, fd.Login)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				resp, err := json.Marshal(map[string]interface{}{
					"token": tokenString,
				})
				if err != nil {
					http.Error(w, `Marshaling Error`, http.StatusInternalServerError)
					return
				}

				w.Write(resp)
				return
			} else {
				ErrBadPassOrLogin := errors.New("invalid login or password")
				w.WriteHeader(http.StatusUnauthorized)
				jsonError(w, http.StatusUnauthorized, ErrBadPassOrLogin.Error())
				return
			}
		}
	}
	// http.Redirect(w, r, "/login", 302) // Unfortunately, redirect dosen't work, cause front has his own SPA routing
}

func jsonError(w http.ResponseWriter, status int, msg string) {
	resp, err := json.Marshal(map[string]interface{}{
		"message": msg,
	})
	if err != nil {
		http.Error(w, `Marshaling Error`, http.StatusInternalServerError)
		return
	}
	w.Write(resp)
}

func queryRowErrorHandler(err error, w http.ResponseWriter) { //
	if err != sql.ErrNoRows {
		http.Error(w, `DB err`, http.StatusInternalServerError)
	} else if err == sql.ErrNoRows {
		http.Error(w, `Don't know you`, http.StatusInternalServerError)
	}
}
