package handlers

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http/httptest"
	"rc/pkg/session"
	"rc/pkg/user"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/golang/mock/gomock"
	"go.uber.org/zap"
)

func TestUserHandlerLogin(t *testing.T) {

	// мы передаём t сюда, это надо чтобы получить корректное сообщение если тесты не пройдут
	ctrl := gomock.NewController(t)

	// Finish сравнит последовательсноть вызовов и выведет ошибку если последовательность другая
	defer ctrl.Finish()

	st := NewMockUserRepositoryInterface(ctrl)
	sessions := NewMockSessionRepositoryInterface(ctrl)
	service := &HandlerDB{
		UserRepo: st,
		Logger:   zap.NewNop().Sugar(), // не пишет логи
		Sessions: sessions,
	}

	resultItems := &user.User{ID: 1, Login: "rvasily", Password: "love"}
	resultFindSession := &session.SQLSession{ID: "AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4", UserID: "1", UserLogin: "rvasily"}
	resultAuthorize := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEyNTc5MzAwMDAsImlhdCI6MTI1Nzg5NDAwMCwidXNlciI6eyJpZCI6IjEiLCJ1c2VybmFtZSI6InJ2YXNpbHkifX0.Wy4RT9H7qf74zgC0KFb83Av828hrel39hVqTgMLgD9A"

	// тут мы записываем последовтаельность вызовов и результат
	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(resultFindSession, nil)
	st.EXPECT().FindUser("1").Return(resultItems, nil)
	sessions.EXPECT().InsertSession(gomock.AssignableToTypeOf(""), "1", "rvasily").Return(nil)
	st.EXPECT().Authorize("1", "rvasily").Return(resultAuthorize, nil)

	// req := httptest.NewRequest("GET", "/", nil)
	reqPayload, err := json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b := bytes.NewBuffer(reqPayload)
	req := httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w := httptest.NewRecorder()

	service.Login(w, req)

	rightResp, err := json.Marshal(map[string]interface{}{
		"token": resultAuthorize,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if len(rightResp) != len(body) {
		t.Errorf("bad authorize token")
		return
	}

	for i := range rightResp {
		if rightResp[i] != body[i] {
			t.Errorf("bad authorize token")
			return
		}
	}

	resultItems = &user.User{ID: 1, Login: "rvasily", Password: "wrong_password"}
	resultFindSession = &session.SQLSession{ID: "AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4", UserID: "1", UserLogin: "rvasily"}

	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(resultFindSession, nil)
	st.EXPECT().FindUser("1").Return(resultItems, nil)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 401 {
		t.Errorf("expected resp status 401, got %d", resp.StatusCode)
		return
	}

	resultItems = &user.User{ID: 1, Login: "rvasily", Password: "wrong_password"}

	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(nil, sql.ErrNoRows)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	resultItems = &user.User{ID: 2, Login: "rvasily", Password: "wrong_password"}
	resultFindSession = &session.SQLSession{ID: "AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4", UserID: "2", UserLogin: "rvasily"}

	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(resultFindSession, nil)
	st.EXPECT().FindUser("2").Return(nil, sql.ErrNoRows)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(nil, sql.ErrTxDone)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	resultItems = &user.User{ID: 1, Login: "rvasily", Password: "love"}
	resultFindSession = &session.SQLSession{ID: "AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4", UserID: "1", UserLogin: "rvasily"}

	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(resultFindSession, nil)
	st.EXPECT().FindUser("1").Return(resultItems, nil)
	sessions.EXPECT().InsertSession(gomock.AssignableToTypeOf(""), "1", "rvasily").Return(sql.ErrTxDone)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	req = httptest.NewRequest("POST", "/api/login", nil)
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 400 {
		t.Errorf("expected resp status 400, got %d", resp.StatusCode)
		return
	}

	sessions.EXPECT().FindSession("AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4").Return(resultFindSession, nil)
	st.EXPECT().FindUser("1").Return(resultItems, nil)
	sessions.EXPECT().InsertSession(gomock.AssignableToTypeOf(""), "1", "rvasily").Return(nil)
	st.EXPECT().Authorize("1", "rvasily").Return("", jwt.ErrInvalidKeyType)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/login", b)
	req.Header.Set("Cookie", "session_id=AAIEBggKDA4QEhQWGBocHiAiJCYoKiwuMDI0Njg6PD4")
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

}

func TestUserHandlerRegister(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	st := NewMockUserRepositoryInterface(ctrl)
	sessions := NewMockSessionRepositoryInterface(ctrl)
	service := &HandlerDB{
		UserRepo: st,
		Sessions: sessions,
	}

	resultRegistration := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEyNTc5MzAwMDAsImlhdCI6MTI1Nzg5NDAwMCwidXNlciI6eyJpZCI6IjEiLCJ1c2VybmFtZSI6InJ2YXNpbHkifX0.Wy4RT9H7qf74zgC0KFb83Av828hrel39hVqTgMLgD9A"

	// тут мы записываем последовтаельность вызовов и результат
	st.EXPECT().Registration("rvasily", "love2love").Return(resultRegistration, "1", nil)
	sessions.EXPECT().Create(gomock.Any(), "1", "rvasily").Return(nil)

	reqPayload, err := json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love2love",
	})
	b := bytes.NewBuffer(reqPayload)
	req := httptest.NewRequest("POST", "/api/register", b)

	w := httptest.NewRecorder()

	service.Register(w, req)

	rightResp, err := json.Marshal(map[string]interface{}{
		"token": resultRegistration,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if len(rightResp) != len(body) {
		t.Errorf("bad authorize token")
		return
	}

	for i := range rightResp {
		if rightResp[i] != body[i] {
			t.Errorf("bad authorize token")
			return
		}
	}

	usernameAlreadyExists := []*user.Error{&user.Error{Location: "body", Msg: "already exists", Param: "username", Value: "rvasily"}}
	st.EXPECT().Registration("rvasily", "!s4:F8sd").Return("", "", usernameAlreadyExists)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "!s4:F8sd",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/register", b)

	w = httptest.NewRecorder()

	service.Register(w, req)

	req = httptest.NewRequest("POST", "/api/register", nil)
	w = httptest.NewRecorder()

	service.Register(w, req)

	resp = w.Result()
	if resp.StatusCode != 400 {
		t.Errorf("expected resp status 400, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().Registration("rvasily", "love2love").Return(resultRegistration, "1", nil)
	sessions.EXPECT().Create(gomock.Any(), "1", "rvasily").Return(sql.ErrTxDone)

	reqPayload, err = json.Marshal(map[string]interface{}{
		"username": "rvasily",
		"password": "love2love",
	})
	b = bytes.NewBuffer(reqPayload)
	req = httptest.NewRequest("POST", "/api/register", b)

	w = httptest.NewRecorder()

	service.Register(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	sess := &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("GET", "/", nil)
	ctx := context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.Index(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 302 {
		t.Errorf("expected resp status 302, got %d", resp.StatusCode)
		return
	}

	req = httptest.NewRequest("GET", "/", nil)
	w = httptest.NewRecorder()
	service.Index(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}
}
