package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	posts "rc/pkg/newPosts"
	"rc/pkg/session"
	"rc/pkg/user"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"

	_ "github.com/go-sql-driver/mysql"
)

//go:generate mockgen -source=posts.go -destination=posts_mock.go -package=handlers posts.PostsDatabaseI

type PostsDatabaseI interface {
	GetSpecificPosts(context.Context, interface{}) ([]*posts.Post, error)
	Create(context.Context, *posts.Post) (interface{}, error)
	FindOne(context.Context, interface{}) (*posts.Post, error)
	DeleteByID(ctx context.Context, filter interface{}) (int64, error)
	UpdatePost(ctx context.Context, filter interface{}, update interface{}) (*mongo.UpdateResult, error)
}

type MongoPostsHandler struct {
	DB     PostsDatabaseI
	Sess   *mongo.Session // idk why its needed
	Posts  *mongo.Collection
	Logger *zap.SugaredLogger
}

func (h *HandlerDB) Index(w http.ResponseWriter, r *http.Request) {
	_, err := session.SessionFromContext(r.Context())
	if err == nil {
		http.Redirect(w, r, "/", 302)
		return
	}

	http.ServeFile(w, r, "../../template/index.html")
}

func (h *MongoPostsHandler) GetAllPosts(w http.ResponseWriter, r *http.Request) {
	posts, err := h.DB.GetSpecificPosts(context.Background(), bson.M{})
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json") //
	json, _ := json.Marshal(posts)
	w.Write(json)
}

func (h *MongoPostsHandler) AddPost(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()

	post := &posts.Post{}
	err := json.Unmarshal(body, post)
	if err != nil {
		jsonError(w, http.StatusBadRequest, "cant unpack payload")
		return
	}

	sess, _ := session.SessionFromContext(r.Context())

	objectUserID, err := primitive.ObjectIDFromHex(sess.UserID)
	if err != nil {
		log.Println("Invalid UserID", objectUserID)
		http.Error(w, `Invalid session id`, http.StatusInternalServerError)
		return
	}
	post.Author = &posts.Author{ID: objectUserID, Username: sess.UserLogin}

	t := time.Now()
	post.Created = t.Format(time.RFC3339)
	post.Comments = []*posts.Comment{}
	post.ID = primitive.NewObjectID()
	post.Score = 1
	post.UpvotePercentage = 100
	post.Votes = []map[string]string{{"user": sess.UserID, "vote": "1"}}

	res, err := h.DB.Create(context.Background(), post)
	if err != nil {
		http.Error(w, `DB error: cannot insert document to collection`, http.StatusInternalServerError)
		return
	}

	fmt.Println("inserted document with ID \n", res)

	resp, err := json.Marshal(post)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Write(resp)
}

func (h *MongoPostsHandler) GetPostByID(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("Invalid id")
	}

	post, err := h.DB.FindOne(context.Background(), bson.M{"_id": objectId})
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	json, _ := json.Marshal(post)
	w.Write(json)
}

func (h *MongoPostsHandler) DeletePost(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("Invalid id")
	}

	res, err := h.DB.DeleteByID(context.Background(), bson.M{"_id": objectId})
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	fmt.Printf("deleted %v documents\n", res)

	resp, err := json.Marshal(map[string]interface{}{
		"message": "success",
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Write(resp)
}

func (h *MongoPostsHandler) DeleteComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID := vars["id"]
	commentID := vars["commentID"]

	objectId, err := primitive.ObjectIDFromHex(postID)
	if err != nil {
		log.Println("Invalid id")
	}

	commentObjectID, err := primitive.ObjectIDFromHex(commentID)
	if err != nil {
		log.Println("Invalid id")
	}

	filter := bson.D{{"_id", objectId}}
	update := bson.D{
		{"$pull",
			bson.D{
				{"comments",
					bson.M{"_id": commentObjectID},
				},
			},
		},
	}
	result, err := h.DB.UpdatePost(context.Background(), filter, update)
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	if result.MatchedCount != 0 {
		h.Logger.Infof("matched and added comment to existing post")
		post, err := h.DB.FindOne(context.Background(), bson.M{"_id": objectId})
		if err != nil {
			http.Error(w, `DB error`, http.StatusInternalServerError)
			return
		}
		json, _ := json.Marshal(post)
		w.Write(json)
		return
	}
	if result.UpsertedCount != 0 {
		fmt.Printf("not matched post with id %s, inserted a new document with ID %v\n", postID, result.UpsertedID)
	}
}

func (h *MongoPostsHandler) GetPostsByCategory(w http.ResponseWriter, r *http.Request) {
	category := mux.Vars(r)["category"]

	posts, err := h.DB.GetSpecificPosts(context.Background(), bson.M{"category": category})
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json") //
	json, _ := json.Marshal(posts)
	w.Write(json)
}

func (h *MongoPostsHandler) AddComment(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	commentBody := ""

	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()

	var objmap map[string]json.RawMessage
	err := json.Unmarshal(body, &objmap)
	if err != nil {
		jsonError(w, http.StatusBadRequest, "cant unpack payload")
		return
	}

	err = json.Unmarshal(objmap["comment"], &commentBody)
	if commentBody == "" {
		EmptyCommentError := []*user.Error{&user.Error{Location: "body", Param: "comment", Msg: "is required"}}
		w.WriteHeader(http.StatusUnprocessableEntity)
		resp, err := json.Marshal(map[string][]*user.Error{"errors": EmptyCommentError})
		if err != nil {
			fmt.Println(err)
			return
		}
		w.Write(resp)
		return
	}

	sess, _ := session.SessionFromContext(r.Context())

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("Invalid id")
	}

	log.Println("provided hex string :", sess.UserID)
	objectUserID, err := primitive.ObjectIDFromHex(sess.UserID)
	if err != nil {
		log.Println("Invalid UserID", err, sess.UserID)
		http.Error(w, `Invalid session id`, http.StatusInternalServerError)
		return
	}

	t := time.Now()
	filter := bson.D{{"_id", objectId}}
	update := bson.D{
		{"$push",
			bson.D{
				{"comments",
					bson.M{"_id": primitive.NewObjectID(),
						"body":    commentBody,
						"created": t.Format(time.RFC3339),
						"author": bson.M{
							"_id":      objectUserID,
							"username": sess.UserLogin},
					},
				},
			},
		},
	}

	result, err := h.DB.UpdatePost(context.Background(), filter, update)
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	if result.MatchedCount != 0 {
		h.Logger.Infof("matched and added comment to existing post")
		post, err := h.DB.FindOne(context.Background(), bson.M{"_id": objectId})
		if err != nil {
			http.Error(w, `DB error`, http.StatusInternalServerError)
			return
		}
		json, _ := json.Marshal(post)
		w.Write(json)
		return
	}
	if result.UpsertedCount != 0 {
		fmt.Printf("not matched post with id %s, inserted a new document with ID %v\n", id, result.UpsertedID)
	}
}

func (h *MongoPostsHandler) GetUserPosts(w http.ResponseWriter, r *http.Request) {
	login := mux.Vars(r)["login"]

	posts, err := h.DB.GetSpecificPosts(context.Background(), bson.M{"author.username": login})
	if err != nil {
		http.Error(w, `DB error`, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json") //
	json, _ := json.Marshal(posts)
	w.Write(json)
}
