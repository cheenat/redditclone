package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http/httptest"
	posts "rc/pkg/newPosts"
	"rc/pkg/session"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func TestPostHandler(t *testing.T) {

	// мы передаём t сюда, это надо чтобы получить корректное сообщение если тесты не пройдут
	ctrl := gomock.NewController(t)

	// Finish сравнит последовательсноть вызовов и выведет ошибку если последовательность другая
	defer ctrl.Finish()

	st := NewMockPostsDatabaseI(ctrl)
	service := &MongoPostsHandler{
		DB:     st,
		Logger: zap.NewNop().Sugar(), // не пишет логи
	}

	ti := time.Now()
	resultItems := []*posts.Post{
		&posts.Post{
			Author:   &posts.Author{ID: primitive.NewObjectID(), Username: "banana"},
			Category: "funny",
			Comments: []*posts.Comment{
				&posts.Comment{
					ID: primitive.NewObjectID(),
					Author: &posts.Author{
						ID:       primitive.NewObjectID(),
						Username: "banana",
					},
					Body:    "comment",
					Created: ti.Format(time.RFC3339),
				},
			},
			Created:          ti.Format(time.RFC3339),
			ID:               primitive.NewObjectID(),
			Score:            151,
			Text:             "Now lemme tell ya something, young blood.",
			Title:            "FizzleDizzle",
			Type:             "text",
			UpvotePercentage: 79,
			Views:            4169,
		},
	}

	st.EXPECT().GetSpecificPosts(context.Background(), bson.M{}).Return(nil, nil)

	req := httptest.NewRequest("GET", "/api/posts/", nil)
	w := httptest.NewRecorder()
	service.GetAllPosts(w, req)

	resp := w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().GetSpecificPosts(context.Background(), bson.M{}).Return(nil, mongo.ErrNilDocument)

	req = httptest.NewRequest("GET", "/api/posts/", nil)
	w = httptest.NewRecorder()
	service.GetAllPosts(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	st.EXPECT().Create(context.Background(), gomock.AssignableToTypeOf(&posts.Post{})).Return(gomock.Any(), nil)

	reqPayload, _ := json.Marshal(map[string]interface{}{
		"category": "music",
		"text":     "text",
		"title":    "title",
		"type":     "text",
	})
	b := bytes.NewBuffer(reqPayload)
	sess := &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/posts", b)
	ctx := context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddPost(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().Create(context.Background(), gomock.AssignableToTypeOf(&posts.Post{})).Return(nil, errors.New("no collection"))

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"category": "music",
		"text":     "text",
		"title":    "title",
		"type":     "text",
	})
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/posts", b)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddPost(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"category": "music",
		"text":     "text",
		"title":    "title",
		"type":     "text",
	})
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/posts", b)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddPost(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	objectId, err := primitive.ObjectIDFromHex("4")
	if err != nil {
		log.Println("Invalid id")
	}
	st.EXPECT().FindOne(context.Background(), bson.M{"_id": objectId}).Return(nil, nil)

	vars := map[string]string{
		"id": "4",
	}
	req = httptest.NewRequest("GET", "/api/post/4", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.GetPostByID(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().FindOne(context.Background(), bson.M{"_id": objectId}).Return(nil, mongo.ErrNoDocuments)
	req = httptest.NewRequest("GET", "/api/post/4", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.GetPostByID(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	st.EXPECT().DeleteByID(context.Background(), bson.M{"_id": objectId}).Return(int64(0), nil)

	vars = map[string]string{
		"id": "4",
	}
	req = httptest.NewRequest("DELETE", "/api/post/4", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.DeletePost(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().DeleteByID(context.Background(), bson.M{"_id": objectId}).Return(int64(0), mongo.ErrNilDocument)

	vars = map[string]string{
		"id": "4",
	}
	req = httptest.NewRequest("DELETE", "/api/post/4", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.DeletePost(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	commentObjectID, err := primitive.ObjectIDFromHex("2")
	if err != nil {
		log.Println("Invalid id")
	}
	update := bson.D{
		{"$pull",
			bson.D{
				{"comments",
					bson.M{"_id": commentObjectID},
				},
			},
		},
	}
	result := &mongo.UpdateResult{
		MatchedCount:  1,
		UpsertedCount: 0,
	}
	st.EXPECT().
		UpdatePost(context.Background(), bson.D{{"_id", objectId}}, update).
		Return(result, nil)
	st.EXPECT().
		FindOne(context.Background(), bson.M{"_id": objectId}).
		Return(nil, nil)

	vars = map[string]string{
		"id":        "4",
		"commentID": "2",
	}
	req = httptest.NewRequest("DELETE", "/api/post/4/2", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.DeleteComment(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().
		UpdatePost(context.Background(), bson.D{{"_id", objectId}}, update).
		Return(nil, mongo.ErrNilDocument)
	vars = map[string]string{
		"id":        "4",
		"commentID": "2",
	}
	req = httptest.NewRequest("DELETE", "/api/post/4/2", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.DeleteComment(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().
		UpdatePost(context.Background(), bson.D{{"_id", objectId}}, update).
		Return(result, nil)
	st.EXPECT().FindOne(context.Background(), bson.M{"_id": objectId}).Return(nil, mongo.ErrNoDocuments)

	vars = map[string]string{
		"id":        "4",
		"commentID": "2",
	}
	req = httptest.NewRequest("DELETE", "/api/post/4/2", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.DeleteComment(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	st.EXPECT().GetSpecificPosts(context.Background(), bson.M{"category": "music"}).Return(resultItems, nil)

	vars = map[string]string{
		"category": "music",
	}
	req = httptest.NewRequest("GET", "/api/posts/music", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.GetPostsByCategory(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().GetSpecificPosts(context.Background(), bson.M{"category": "music"}).Return(nil, mongo.ErrNilDocument)

	vars = map[string]string{
		"category": "music",
	}
	req = httptest.NewRequest("GET", "/api/posts/music", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.GetPostsByCategory(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	st.EXPECT().GetSpecificPosts(context.Background(), bson.M{"author.username": "cheena"}).Return(resultItems, nil)

	vars = map[string]string{
		"login": "cheena",
	}
	req = httptest.NewRequest("GET", "/api/user/cheena", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.GetUserPosts(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().GetSpecificPosts(context.Background(), bson.M{"author.username": "cheena"}).Return(nil, mongo.ErrNilDocument)

	vars = map[string]string{
		"login": "cheena",
	}
	req = httptest.NewRequest("GET", "/api/user/cheena", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.GetUserPosts(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	filter := bson.D{{"_id", objectId}}

	st.EXPECT().
		UpdatePost(context.Background(), filter, gomock.AssignableToTypeOf(bson.D{})).
		Return(result, nil)
	st.EXPECT().
		FindOne(context.Background(), bson.M{"_id": objectId}).
		Return(nil, nil)

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"comment": "funny comment",
	})
	vars = map[string]string{
		"id": "4",
	}
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/post/5", b)
	req = mux.SetURLVars(req, vars)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddComment(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().UpdatePost(context.Background(), filter, gomock.AssignableToTypeOf(bson.D{})).Return(result, nil)
	st.EXPECT().FindOne(context.Background(), bson.M{"_id": objectId}).Return(nil, mongo.ErrNoDocuments)

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"comment": "funny comment",
	})
	vars = map[string]string{
		"id": "4",
	}
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/post/5", b)
	req = mux.SetURLVars(req, vars)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddComment(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"comment": "funny comment",
	})
	vars = map[string]string{
		"id": "4",
	}
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/post/5", b)
	req = mux.SetURLVars(req, vars)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddComment(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"comment": "",
	})
	vars = map[string]string{
		"id": "4",
	}
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/post/5", b)
	req = mux.SetURLVars(req, vars)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddComment(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 422 {
		t.Errorf("expected resp status 422, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().
		UpdatePost(context.Background(), filter, gomock.AssignableToTypeOf(bson.D{})).
		Return(nil, mongo.ErrNilDocument)

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"comment": "funny comment",
	})
	vars = map[string]string{
		"id": "4",
	}
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/post/5", b)
	req = mux.SetURLVars(req, vars)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddComment(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	st.EXPECT().GetSpecificPosts(context.Background(), gomock.Any()).Return(nil, nil)

	req = httptest.NewRequest("GET", "/api/user/cheena", nil)
	w = httptest.NewRecorder()
	service.GetUserPosts(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	st.EXPECT().GetSpecificPosts(context.Background(), gomock.Any()).Return(nil, mongo.ErrNilDocument)

	req = httptest.NewRequest("GET", "/api/user/cheena", nil)
	w = httptest.NewRecorder()
	service.GetUserPosts(w, req)

	resp = w.Result()
	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	//

	result.MatchedCount = 0
	result.UpsertedCount = 1
	st.EXPECT().
		UpdatePost(context.Background(), bson.D{{"_id", objectId}}, update).
		Return(result, nil)
	vars = map[string]string{
		"id":        "4",
		"commentID": "2",
	}
	req = httptest.NewRequest("DELETE", "/api/post/4/2", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()
	service.DeleteComment(w, req)

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	//

	st.EXPECT().
		UpdatePost(context.Background(), filter, gomock.AssignableToTypeOf(bson.D{})).
		Return(result, nil)

	reqPayload, _ = json.Marshal(map[string]interface{}{
		"comment": "funny comment",
	})
	vars = map[string]string{
		"id": "4",
	}
	b = bytes.NewBuffer(reqPayload)
	sess = &session.SQLSession{
		ID:        "4",
		UserID:    "5fe5e3f22904a88e666b1bce",
		UserLogin: "cheena",
	}
	req = httptest.NewRequest("POST", "/api/post/5", b)
	req = mux.SetURLVars(req, vars)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()
	service.AddComment(w, req.WithContext(ctx))

	resp = w.Result()
	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}
}
