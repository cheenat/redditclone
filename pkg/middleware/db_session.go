package middleware

// import (
// 	"context"
// 	"fmt"
// 	"net/http"
// 	posts "rc/pkg/newPosts"

// 	"github.com/gin-gonic/gin"
// 	"go.mongodb.org/mongo-driver/mongo"
// )

// // ALL MIDDLEWARES PACKAGE CAN BE REMOVED IF user2020 HANDLERS ARE USED

// const dbContextName = "db"

// type DatabaseSession struct {
// 	DB posts.DatabaseHelper
// }

// func GetDbSessionContext(c *gin.Context) mongo.SessionContext {
// 	return c.MustGet(dbContextName).(mongo.SessionContext)
// }

// func (d DatabaseSession) SetDBContext(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		fmt.Println("setDBContextMiddleware", r.URL.Path)
// 		session, err := d.DB.Client().StartSession()
// 		if err != nil {
// 			http.Error(w, "Internal server error", 500)
// 			return
// 		}
// 		mongo.WithSession(context.Background(), session, func(sessionContext mongo.SessionContext) error {
// 			// c.Set(dbContextName, sessionContext)
// 			next.ServeHTTP(w, r)
// 			return nil
// 		})
// 	})
// }

// func (d DatabaseSession) SetDbContext(c *gin.Context) {
// 	session, err := d.DB.Client().StartSession()
// 	if err != nil {
// 		c.AbortWithError(500, err)
// 		return
// 	}
// 	mongo.WithSession(c, session, func(sessionContext mongo.SessionContext) error {
// 		c.Set(dbContextName, sessionContext)
// 		c.Next()
// 		return nil
// 	})

// }

// // func (d DatabaseSession) SetDbContext(c *gin.Context) {
// // 	session, err := d.DB.Client().StartSession()
// // 	if err != nil {
// // 		c.AbortWithError(500, err)
// // 		return
// // 	}
// // 	mongo.WithSession(c, session, func(sessionContext mongo.SessionContext) error {
// // 		c.Set(dbContextName, sessionContext)
// // 		c.Next()
// // 		return nil
// // 	})

// // }
