package middleware

import (
	"fmt"
	"net/http"
	"time"

	"go.uber.org/zap"
)

func AccessLogWrapper(logger *zap.SugaredLogger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Println("access log middleware")
			start := time.Now()
			next.ServeHTTP(w, r)
			logger.Infow("New request",
				"method", r.Method,
				"remote_addr", r.RemoteAddr,
				"url", r.URL.Path,
				"time", time.Since(start),
			)
		})
	}
}
