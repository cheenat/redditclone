package middleware

import (
	"context"
	"net/http"

	"rc/pkg/session"

	"github.com/gorilla/mux"
)

var (
	noSessUrls = map[string]struct{}{
		"/":                     struct{}{},
		"/static/":              struct{}{},
		"/api/login":            struct{}{}, // must be commented, but redirect dosen't work cause front has his own SPA routing
		"/api/register":         struct{}{},
		"/api/posts/":           struct{}{},
		"/api/posts/{category}": struct{}{},
	}
)

func AuthWrapper(sm *session.SessionsManager) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			routeTemplate, _ := mux.CurrentRoute(r).GetPathTemplate()

			sess, err := sm.Check(r)
			_, canbeWithouthSess := noSessUrls[routeTemplate]
			if err != nil && !canbeWithouthSess {
				// http.Redirect(w, r, "/login", 302)
				return
			}

			ctx := context.WithValue(r.Context(), session.SessionKey, sess)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
