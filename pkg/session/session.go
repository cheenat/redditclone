package session

import (
	"context"
	"errors"
)

type SQLSession struct {
	ID        string
	UserID    string
	UserLogin string
}

var (
	ErrNoAuth = errors.New("No session found")
)

type sessKey string

var SessionKey sessKey = "sessionKey"

func SessionFromContext(ctx context.Context) (*SQLSession, error) {
	sess, ok := ctx.Value(SessionKey).(*SQLSession)
	if !ok || sess == nil {
		return nil, ErrNoAuth
	}
	return sess, nil
}
