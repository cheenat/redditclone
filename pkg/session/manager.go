package session

import (
	"database/sql"
	"fmt"
	"net/http"
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
)

type SessionsManager struct {
	mu *sync.RWMutex
	DB *sql.DB
}

func NewSessionsMem(db *sql.DB) *SessionsManager {
	return &SessionsManager{
		mu: &sync.RWMutex{},
		DB: db,
	}
}

func (sm *SessionsManager) Check(r *http.Request) (*SQLSession, error) {
	sessionCookie, err := r.Cookie("session_id")
	if err == http.ErrNoCookie {
		return nil, ErrNoAuth
	}

	sessFromSQL := &SQLSession{}
	sqlStmt := `SELECT id, userID, login FROM sessions WHERE id = ?`
	err = sm.DB.QueryRow(sqlStmt, sessionCookie.Value).Scan(&sessFromSQL.ID, &sessFromSQL.UserID, &sessFromSQL.UserLogin)
	if err != nil {
		if err != sql.ErrNoRows {
			// real error happened
		}
		return nil, ErrNoAuth
	} else if sessFromSQL.UserLogin == "" {

		return nil, ErrNoAuth
	}

	return sessFromSQL, nil
}

func (sm *SessionsManager) Create(w http.ResponseWriter, userID string, login string) error {
	sessID := uuid.NewV4()
	cookie := &http.Cookie{
		Name:    "session_id",
		Value:   sessID.String(),
		Expires: time.Now().Add(90 * 24 * time.Hour),
		Path:    "/",
	}
	http.SetCookie(w, cookie)

	result, err := sm.DB.Exec(
		"INSERT INTO sessions (`id`, `userID`, `login`) VALUES (?, ?, ?)",
		sessID.String(),
		userID,
		login,
	)
	if err != nil {
		return err
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	lastID, err := result.LastInsertId()
	if err != nil {
		return err
	}
	fmt.Println("Insert - RowsAffected", affected, "LastInsertId: ", lastID)

	return nil
}

func (sm *SessionsManager) FindSession(id string) (*SQLSession, error) {
	var session SQLSession
	fmt.Println("session id", id, session.UserID, sm.DB)
	err := sm.DB.QueryRow(`SELECT userID FROM sessions WHERE id = ?`, id).Scan(&session.UserID)
	if err != nil {
		return nil, err
	}
	return &session, nil
}

func (sm *SessionsManager) InsertSession(sessID, userID, userLogin string) error {
	result, err := sm.DB.Exec(
		"INSERT INTO sessions (`id`, `userID`, `login`) VALUES (?, ?, ?)",
		sessID,
		userID,
		userLogin,
	)
	if err != nil {
		return err
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	lastID, err := result.LastInsertId()
	if err != nil {
		return err
	}

	fmt.Println("Insert - RowsAffected", affected, "LastInsertId: ", lastID)
	return nil
}

/*func (sm *SessionsManager) DestroyCurrent(w http.ResponseWriter, r *http.Request) error {
	sess, err := SessionFromContext(r.Context())
	if err != nil {
		return err
	}

	sm.mu.Lock()
	delete(sm.data, sess.ID.String())
	sm.mu.Unlock()

	cookie := http.Cookie{
		Name:    "session_id",
		Expires: time.Now().AddDate(0, 0, -1),
		Path:    "/",
	}
	http.SetCookie(w, &cookie)
	return nil
}*/
