# Redditclone

Backend for a redditclone on Golang with MySQL and MongoDB databases

### Prerequisites

```
install golang: https://golang.org/doc/install
install docker-compose: https://docs.docker.com/compose/install/
```

### Installing

```
git clone https://gitlab.com/cheenat/redditclone.git
cd redditclone
go mod download
```


### Getting Started

```
cd redditclone
docker-compose up
cd cmd/cheenasclone/
go run main.go
```

Go to http://localhost:8080 in your browser, and you should see reddit main page

### Running the tests

running all test in main directory

```
go test -v -coverprofile cover.out ./...
go tool cover -html=cover.out -o cover.html
open cover.html
```

### Built binary file

```
go build -o bin/cheenasclone ./cmd/cheenasclone/main.go 
```