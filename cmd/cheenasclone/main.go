package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"rc/pkg/handlers"
	"rc/pkg/middleware"
	posts "rc/pkg/newPosts"
	"rc/pkg/session"
	"rc/pkg/user"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

// run only from terminal in the same directory as "static"
func main() {
	dsn := "root:1234@tcp(localhost:3306)/redditclone?"
	dsn += "&charset=utf8"
	db, err := sql.Open("mysql", dsn)
	db.SetMaxOpenConns(10)

	err = db.Ping()
	if err != nil {
		log.Fatal("MySQL database is not runnign on port 3306: ", err)
	}

	result, err := db.Exec(`CREATE TABLE IF NOT EXISTS users (
		id int(11) NOT NULL AUTO_INCREMENT,
		login varchar(200) NOT NULL,
		password varchar(200) NOT NULL,
		userID varchar(220) NOT NULL,
		PRIMARY KEY (id)
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`)
	if err != nil {
		log.Fatal(err)
	}

	result, err = db.Exec(`CREATE TABLE IF NOT EXISTS sessions (
		id varchar(200) NOT NULL,
		userID varchar(220) NOT NULL,
		login varchar(200) NOT NULL
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`)
	if err != nil {
		log.Fatal(err)
	}

	affected, err := result.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	lastID, err := result.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Insert - RowsAffected", affected, "LastInsertId: ", lastID)

	_, err = db.Exec(`INSERT IGNORE INTO users (id, login, password, userID) VALUES
		(1,	'12',	'love2love', '4af9f07012df8501822718f0'),
		(2,	'admin',	'admin123', '4af9f07012df8501822718f1');`)
	if err != nil {
		log.Fatal(err)
	}

	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)

	redditclone := client.Database("redditclone")
	postsCollection := redditclone.Collection("posts")

	count, err := postsCollection.CountDocuments(context.TODO(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("name Bob appears in %v documents", count)
	if count == 0 {
		t := time.Now()
		_, _ = postsCollection.InsertMany(ctx, []interface{}{
			bson.M{
				"author":           bson.M{"_id": primitive.NewObjectID(), "username": "banana"},
				"category":         "funny",
				"comments":         bson.A{bson.M{"_id": primitive.NewObjectID(), "body": "comment", "created": t.Format(time.RFC3339), "author": bson.M{"_id": primitive.NewObjectID(), "username": "banana"}}},
				"created":          t.Format(time.RFC3339),
				"_id":              primitive.NewObjectID(),
				"score":            151,
				"text":             "Now lemme tell ya something, young blood.",
				"title":            "FizzleDizzle",
				"type":             "text",
				"upvotePercentage": 79,
				"views":            4169,
			},
			bson.M{
				"author":           bson.M{"_id": primitive.NewObjectID(), "username": "not banana"},
				"category":         "music",
				"created":          t.Format(time.RFC3339),
				"_id":              primitive.NewObjectID(),
				"score":            -1,
				"url":              "http://127.0.0.1:8080/",
				"title":            "title",
				"type":             "link",
				"upvotePercentage": 79,
				"views":            4169,
			},
		})
	}

	sm := session.NewSessionsMem(db)
	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync() // flushes buffer, if any
	logger := zapLogger.Sugar()

	userRepo := user.NewUserRepo(db)

	handlersDB := &handlers.HandlerDB{
		UserRepo: userRepo,
		Sessions: sm,
	}

	mongoDBWrapper := &posts.MongoDatabase{
		DB: redditclone,
	}

	mongoDB := posts.NewPostDatabase(mongoDBWrapper)
	mongodbHandlers := &handlers.MongoPostsHandler{
		DB:     mongoDB,
		Posts:  postsCollection,
		Logger: logger,
	}

	r := mux.NewRouter()
	r.HandleFunc("/", handlersDB.Index).Methods("GET")
	r.HandleFunc("/api/register", handlersDB.Register).Methods("POST")
	r.HandleFunc("/api/login", handlersDB.Login).Methods("POST")
	r.HandleFunc("/api/posts/", mongodbHandlers.GetAllPosts).Methods("GET")
	r.HandleFunc("/api/posts", mongodbHandlers.AddPost).Methods("POST")
	r.HandleFunc("/api/post/{id}", mongodbHandlers.GetPostByID).Methods("GET")
	r.HandleFunc("/api/post/{id}", mongodbHandlers.DeletePost).Methods("DELETE")
	r.HandleFunc("/api/post/{id}", mongodbHandlers.AddComment).Methods("POST")
	r.HandleFunc("/api/post/{id}/{commentID}", mongodbHandlers.DeleteComment).Methods("DELETE")
	r.HandleFunc("/api/posts/{category}", mongodbHandlers.GetPostsByCategory).Methods("GET")
	r.HandleFunc("/api/user/{login}", mongodbHandlers.GetUserPosts).Methods("GET")

	r.PathPrefix("/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("../../template/static"))))

	r.Use(middleware.AuthWrapper(sm), middleware.AccessLogWrapper(logger), middleware.Panic)

	addr := ":8080"
	logger.Infow("starting server",
		"type", "START",
		"addr", addr,
	)

	http.ListenAndServe(addr, r)
}
